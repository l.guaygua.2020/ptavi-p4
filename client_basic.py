#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys
# Constantes. Dirección IP del servidor y contenido a enviar
#SERVER = 'localhost'
#PORT = 6001
#LINE = 'hola'

def main():

    SERVER = sys.argv[1]
    PORT = int(sys.argv[2])
    LINE = ' '.join(sys.argv[3:])

    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.sendto(LINE.encode('utf-8'), (SERVER, PORT))
            data = my_socket.recv(1024)
            print('Recibido: ', data.decode('utf-8'))


            my_socket.sendto(data.decode('utf-8').encode('utf-8'), (SERVER, PORT))
#           data2 = my_socket.recv(1024)
            print('Recibido: ', data.decode('utf-8'))

        print("Cliente terminado.")

    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()