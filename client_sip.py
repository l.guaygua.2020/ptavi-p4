#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys
# Constantes. Dirección IP del servidor y contenido a enviar
#SERVER = 'localhost'
#PORT = 6001
#LINE = 'hola'

def main():

    SERVER = sys.argv[1]
    PORT = int(sys.argv[2])
    REGISTRO = sys.argv[3]
    LINE = sys.argv[4]

    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            PETICION = REGISTRO.upper() + " " + "sip: " + LINE + " " + "SIP/2.0\r\n\r\n"
            my_socket.sendto(PETICION.encode('utf-8'), (SERVER, PORT))  # enviamos mensaje
            data = my_socket.recv(1024)                                # lo que recibo, 1024 es el tamaño dle mensaje
            print(PETICION)
            print(data.decode('utf-8'))               # lo que reciba del servidor
        print("Cliente terminado.")

    except ConnectionRefusedError:
        print("Error conectando a servidor")

# escriba en pantalla lo que recibe el servidos
if __name__ == "__main__":
    main()
