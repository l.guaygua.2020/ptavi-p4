"""Tests for server.py (SIPRequest class)

"""

import unittest

import server_sip

class TestCommand(unittest.TestCase):
    """Tests for _parse_command"""

    def test_simple(self):
        request = server_sip.SIPRequest(b"REGISTER sip:maria@registrar.com SIP/2.0\r\n")
        request._parse_command("REGISTER sip:maria@registrar.com SIP/2.0")
        self.assertEqual("REGISTER", request.command)
        self.assertEqual("maria@registrar.com", request.address)
        self.assertEqual("200 OK", request.result)

    def test_nosip(self):
        request = server_sip.SIPRequest(b"REGISTER sip:maria@registrar.com SIP/2.0\r\n")
        request._parse_command("REGISTER maria@registrar.com SIP/2.0")
        self.assertEqual("REGISTER", request.command)
        self.assertEqual(None, request.address)
        self.assertEqual("416 Unsupported URI Scheme", request.result)

class TestHeaders(unittest.TestCase):
    """Tests for _parse_headers"""

    def test_simple(self):
        first_line = b"REGISTER sip:maria@registrar.com SIP/2.0\r\n"
        request = server_sip.SIPRequest(first_line + b"Expires: 3699\r\n\r\n")
        request._parse_headers(len(first_line))
        self.assertEqual({"Expires": "3699"}, request.headers)

class TestParse(unittest.TestCase):
    """Tests for parse"""

    def test_simple(self):

        request = server_sip.SIPRequest( b"REGISTER sip:maria@registrar.com SIP/2.0\r\n"
                                         + b"Expires: 3699\r\n\r\n")
        request.parse()
        self.assertEqual("REGISTER", request.command)
        self.assertEqual("maria@registrar.com", request.address)
        self.assertEqual("200 OK", request.result)
        self.assertEqual({"Expires": "3699"}, request.headers)
