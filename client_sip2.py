#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys


def main():
    # Constantes. Dirección IP del servidor y contenido a enviar

    SERVER = sys.argv[1]
    PORT = int(sys.argv[2])
    REGISTRO = sys.argv[3]
    LINE = sys.argv[4]
    EXPIRE = int(sys.argv[5])
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            PETICION = REGISTRO.upper() + " sip:" + LINE + " SIP/2.0\r\n" + "Expires: " + str(EXPIRE) + "\r\n\r\n"
            print(PETICION)
            my_socket.sendto(PETICION.encode('utf-8'), (SERVER, PORT))  # enviamos mensaje
            data = my_socket.recv(1024)
            print(data.decode('utf-8'))
        print("Cliente terminado.")
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
