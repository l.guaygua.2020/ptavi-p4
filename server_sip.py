#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys


class SIPRequest:

    def __init__(self, data):
        self.data = data

    def parse(self):
        received = self.data.decode('utf-8')
        # print(received) # Muestra el mensaje mandado en el servidor
        self._parse_command(received)
        primer_nl = received.splitlines()[1:]
        self._parse_headers(primer_nl[0])

    def _get_address(self, uri):  # cortar el mensaje para dividir correo y sip
        cut = uri.split(":")
        address = cut[1]
        schema = cut[0]
        return address, schema

    def _parse_command(self, line):
        cut = line.split(' ')
        self.command = cut[0].upper()
        self.uri = cut[1]
        sip_uri = self._get_address(self.uri)
        self.address = sip_uri[0]
        schema = sip_uri[1]
        if self.command == "REGISTER":

            if schema == "sip":
                self.result = "200 OK"
            else:
                self.result = "416 Unsupported URI Scheme"
        else:
            self.result = "405 Method Not Allowed"

    def _parse_headers(self, first_nl):
        self.headers = {}  # para cabecera
        for line in first_nl:
            split_line = line.split(':')
            if len(split_line) >= 2:
                cut = line.split(':')
                head = cut[0]
                value = cut[1]
                self.headers[head] = value


class SIPRegisterHandler(socketserver.BaseRequestHandler):
    """
    Echo server class
    """
    registro = {}
    def process_register(self, elemento): #no entiendo lo que tiene que hacer esta funcion

        self.registro[self.client_address[0]] = elemento.headers

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        data = self.request[0]  # datos que recivo
        sock = self.request[1]  # socket que puedo usar para responder el cual ya contiene la direccion del cliente
        sip_request = SIPRequest(data)
        sip_request.parse()

        if (sip_request.command == "REGISTER") and (sip_request.result == "200 OK"):
            self.process_register(sip_request)
        sock.sendto(f"SIP/2.0 {sip_request.result}\r\n\r\n".encode(), self.client_address)


def main():
    # Constantes. Puerto.
    PORT = sys.argv[1]

    if len(sys.argv) < 2:
        sys.exit("Error de valores")
    # Listens at port PORT (my address)
    # and calls the EchoHandler class to manage the request
    try:
        # creacion del servidor que cuando reciba mensajes llamara al manejador
        serv = socketserver.UDPServer(('', int(PORT)), SIPRegisterHandler)  # se puede cambiar al int arriba
        # la cadena vacia '' son todas mis direcciones IP y el puerto especificado
        # el EchoHandler sera el manejador del mensaje
        print(f"Server listening in port ({PORT})")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")
    try:
        serv.serve_forever()  # escucha siempre hasta matarlo
        # si le llega un mensaje llama al manejador
    except KeyboardInterrupt:
        # terminar ordenadamente para liberar el puerto
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
